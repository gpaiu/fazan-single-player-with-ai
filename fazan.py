#!/usr/bin/env python3

import os
import random


class Game:
    def __init__(self, players=1, ai=1, ai_dict=None):

        """ Constructor """

        self.__human_players = players
        self.__ai_players = ai
        self.__max_score = 4
        self.__termination = 2
        self._players = []
        self._ai_dictionary = ai_dict
        self._round_number = 0

    def valid_word(self, word):

        """ Determine if a word is valid, based on the dictionary file. """

        with open(self._ai_dictionary) as handler:
            line = handler.readline()
            while line:
                if line.strip() == word:
                    return True
                line = handler.readline()
        return False

    def closing_word(self, word):

        """ Determines if a word starting with 'starting_letters_count' letters can be obtained following 'word'. """

        termination = word[-self.__termination:]
        with open(self._ai_dictionary) as handler:
            line = handler.readline()
            while line:
                if line.strip().lower().startswith(termination.lower()):
                    return False
                line = handler.readline()
        return True

    def _add_players(self):

        """ Adds players based on their number. """

        # Create human and AI players
        for _ in range(0, self.__human_players):
            self._players.append(Player())
        for ai_id in range(0, self.__ai_players):
            self._players.append(AI(self._ai_dictionary, name="AI-%d" % ai_id))

        # Shuffle player order
        random.shuffle(self._players)

    def start(self):

        """ Starts the game. """

        self._add_players()
        while len(self._players) > 1:
            self._round()
        winner = self._players.pop()
        print("Game over! %s has won" % winner.name())

    def _round(self):

        """ Starts a round

        Algorithm:
            - cycle through each player
            - get a word from the current player
            - if the player inputs a blocking word, the next player will have their score incremented

        """

        round_end = False
        closing_word_found = False
        starting_letters = chr(random.randint(97, 122))
        while not round_end:
            for player in self._players:
                if closing_word_found:
                    round_end = True
                    player.score += 1
                    self._round_number += 1
                    print("-- %s lost this round." % player.name())
                    self.score()
                    if player.score > self.__max_score:
                        print("%s has been eliminated" % player.name())
                        self._players.remove(player)
                    break
                word = player.get_word(starting_letters)
                while not self.valid_word(word):
                    print("%s is not a valid word." % word)
                    word = player.get_word()
                print("-- %s said '%s'" % (player.name(), word))
                if self.closing_word(word):
                    closing_word_found = True
                else:
                    starting_letters = word[-self.__termination:]

    def score(self):
        FAZAN = "FAZAN"
        print("-- Current score - ROUND %d" % self._round_number)
        for player in self._players:
            print("---- %s : %s" % (player.name(), FAZAN[0:player.score]))


class Player:

    """ Defines a human player """

    def __init__(self, name=None):

        """ Human player constructor. """

        self._name = name
        self.score = 0
        if not self._name:
            self.get_name_from_user()

    def name(self):

        """ Name getter. """

        return self._name

    def get_word(self, starting_letters=None):

        """ Get a word from a human player """

        if starting_letters:
            word = input("%s, please enter a word starting with %s: " % (self._name, starting_letters))
            while not word.lower().startswith(starting_letters):
                word = input("%s is an invalid word, please enter another: " % word)
            return word
        word = input("%s, please enter a word: " % self._name)
        return word

    def get_name_from_user(self):

        """ Get the name from the user that's playing """

        user_name = None
        while not user_name:
            user_name = input("Please enter your name: ")
        self._name = user_name
        print("Welcome, %s" % self._name)


class AI(Player):

    """ Defines an AI player """

    def __init__(self, dictionary, name="AI"):

        """ AI player constructor """

        super(AI, self).__init__(name)
        self._dictionary = dictionary
        if not os.path.isfile(self._dictionary):
            raise FileExistsError

    def get_word(self, starting_letters=None):

        """ Get a word from the AI player. """

        if starting_letters:
            valid_word_list = []
            with open(self._dictionary) as handler:
                line = handler.readline()
                while line:
                    current_word = line.strip()
                    if current_word.startswith(starting_letters):
                        valid_word_list.append(current_word)
                    line = handler.readline()
            return random.choice(valid_word_list)
        with open(self._dictionary) as handler:
            line = next(handler)
            for num, aline in enumerate(handler, 2):
                if random.randrange(num):
                    continue
                line = aline
            return line.strip()


def main():

    """ Entry point if the program is run from the command line. """

    game = Game(ai_dict="all.txt")
    game.start()


if __name__ == "__main__":
    main()
